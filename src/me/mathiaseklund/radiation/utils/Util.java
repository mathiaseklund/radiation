package me.mathiaseklund.radiation.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.radiation.Main;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

public class Util {

	public static void msg(CommandSender sender, String msg) {
		sender.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
	}

	public static void error(CommandSender sender, String msg) {
		msg(sender, "&4ERROR:&7 " + msg);
	}

	public static void error(String msg) {
		error(Bukkit.getConsoleSender(), msg);
	}

	public static void debug(String msg) {
		if (Main.getMain().getCfg().isDebug()) {
			msg(Bukkit.getConsoleSender(), "&e[&7DEBUG&e]&r " + msg);
		}
	}

	public static void actionBar(Player player, String msg) {
		player.spigot().sendMessage(ChatMessageType.ACTION_BAR,
				new TextComponent(ChatColor.translateAlternateColorCodes('&', msg)));
	}
}
