package me.mathiaseklund.radiation.utils;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import lombok.Getter;
import me.mathiaseklund.radiation.Main;

public class Config {

	Main main;
	File f;
	FileConfiguration fc;

	@Getter
	boolean debug;

	@Getter
	List<String> radiationRegions;

	@Getter
	int radiationInterval, radiationIncrement, radiationHarmRequired, radiationHarmInterval, radiationCap;

	@Getter
	double radiationHarmDamage;
	
	@Getter
	String radiationIncreased;

	public Config() {
		main = Main.getMain();
		f = new File(main.getDataFolder(), "config.yml");
		if (!f.exists()) {
			main.saveResource("config.yml", true);
		}
		fc = YamlConfiguration.loadConfiguration(f);
		load();
	}

	void load() {
		debug = fc.getBoolean("debug", false);
		radiationRegions = fc.getStringList("radiation.regions");
		radiationIncrement = fc.getInt("radiation.increment", 3);
		radiationInterval = fc.getInt("radiation.interval", 20);
		radiationHarmRequired = fc.getInt("radiation.harm.required", 40);
		radiationHarmInterval = fc.getInt("radiation.harm.interval", 30);
		radiationHarmDamage = fc.getDouble("radiation.harm.damage", 3);
		radiationCap = fc.getInt("radiation.cap", 60);
		radiationIncreased = fc.getString("messages.radiation-increased", "Radiation: %radiation%");

	}

	public void save() {
		try {
			fc.save(f);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setRadiationRegions(List<String> list) {
		radiationRegions = list;
		fc.set("radiation.regions", list);
		save();
	}
}
