package me.mathiaseklund.radiation.radiation;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.google.common.collect.Lists;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import lombok.Getter;
import me.mathiaseklund.radiation.Main;
import me.mathiaseklund.radiation.utils.Util;

public class RadiationManager {

	Main main;

	@Getter
	HashMap<UUID, Integer> radiation = new HashMap<>();
	List<UUID> inRadiationRegion = Lists.newArrayList();
	public List<Player> killQueue = Lists.newArrayList();

	public RadiationManager() {
		main = Main.getMain();

		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				processRadiationIncrement();
				processRadiationHarm();
				processKillQueue();
			}
		});
	}

	public List<String> getActiveRegions() {
		return main.getCfg().getRadiationRegions();
	}

	public void setActiveRegions(List<String> list) {
		main.getCfg().setRadiationRegions(list);
	}

	public void toggleRadiation(CommandSender sender, String regionName) {
		List<String> list = getActiveRegions();
		if (list != null) {
			list = Lists.newArrayList();
		}

		if (list.contains(regionName)) {
			list.remove(regionName);
			Util.msg(sender, "&c" + regionName + " is no longer a radiation zone.");
		} else {
			list.add(regionName);
			Util.msg(sender, "&a" + regionName + " is now a radiation zone.");
		}

		setActiveRegions(list);
	}

	public boolean isRadiationRegion(String regionName) {
		return main.getCfg().getRadiationRegions().contains(regionName);
	}

	public boolean isRadiationLocation(Location loc) {
		boolean b = false;

		RegionManager rm = main.getWorldGuard().getRegionManager(loc.getWorld());
		ApplicableRegionSet regions = rm.getApplicableRegions(loc);
		for (ProtectedRegion region : regions) {
			String id = region.getId();
			// Util.debug("Found Region: " + id);
			if (isRadiationRegion(id)) {
				b = true;
				break;
			}
		}
		return b;
	}

	public int getRadiation(Player player) {
		return getRadiation(player.getUniqueId());
	}

	public int getRadiation(UUID uuid) {
		int rad = 0;
		if (radiation.containsKey(uuid)) {
			rad = radiation.get(uuid);
		}
		return rad;
	}

	void processRadiationIncrement() {
		List<UUID> uuids = Lists.newArrayList(radiation.keySet());
		int increment = main.getCfg().getRadiationIncrement();
		for (UUID uuid : uuids) {
			// Util.debug(uuid.toString());
			Player player = Bukkit.getPlayer(uuid);
			if (player != null) {
				if (!player.hasPermission("rmc.region.immune")) {
					if (!player.isDead()) {
						int rad = getRadiation(uuid);
						if (isInRadiationRegion(uuid)) {
							rad += increment;
						} else {
							rad -= increment;
						}

						if (rad > main.getCfg().getRadiationCap()) {
							rad = main.getCfg().getRadiationCap();
						}

						if (rad <= 0) {
							radiation.remove(uuid);
						} else {
							radiation.put(uuid, rad);
						}

						Util.actionBar(player, main.getCfg().getRadiationIncreased().replace("%radiation%", rad + ""));
					} else {
						radiation.remove(uuid);
						inRadiationRegion.remove(uuid);
					}
				}
			}
		}

		Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
			public void run() {
				processRadiationIncrement();
			}
		}, main.getCfg().getRadiationInterval());
	}

	void processRadiationHarm() {
		List<UUID> uuids = Lists.newArrayList(radiation.keySet());
		int required = main.getCfg().getRadiationHarmRequired();
		for (UUID uuid : uuids) {
			int rad = getRadiation(uuid);
			if (rad >= required) {
				Player player = Bukkit.getPlayer(uuid);
				if (player != null) {
					if (!player.isDead()) {
						if (!player.hasPermission("rmc.region.immune")) {
							try {
								player.damage(main.getCfg().getRadiationHarmDamage());
							} catch (Exception e) {
								// do nothing
							}
						}
					}
				}
			}
		}

		Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
			public void run() {
				processRadiationHarm();
			}
		}, main.getCfg().getRadiationHarmInterval());
	}

	public boolean isInRadiationRegion(UUID uuid) {
		return inRadiationRegion.contains(uuid);
	}

	public void enterRadiationArea(UUID uuid) {
		int rad = getRadiation(uuid);
		radiation.put(uuid, rad);
		inRadiationRegion.add(uuid);
	}

	public void leaveRadiationArea(UUID uuid) {
		inRadiationRegion.remove(uuid);
	}

	public void resetRadiation(Player player) {

		radiation.remove(player.getUniqueId());

		if (isInRadiationRegion(player.getUniqueId())) {
			inRadiationRegion.remove(player.getUniqueId());
		}
	}

	void processKillQueue() {
		if (!killQueue.isEmpty()) {
			Player player = killQueue.get(0);
			if (!player.isDead()) {
				player.damage(player.getHealth());
			} else {
				killQueue.remove(player);
			}
		}
		Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
			public void run() {
				processKillQueue();
			}
		}, 1);
	}
}
