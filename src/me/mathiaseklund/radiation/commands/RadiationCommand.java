package me.mathiaseklund.radiation.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import me.mathiaseklund.radiation.Main;
import me.mathiaseklund.radiation.utils.Util;

public class RadiationCommand implements CommandExecutor {

	Main main = Main.getMain();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				if (sender.hasPermission("rmc.admin")) {
					if (args.length == 0) {
						Util.msg(sender, "&7Usage: ");
						Util.msg(sender,
								"&e- /radiation set/toggle <regionName>&f - Toggles active radiation in the region.");
						Util.msg(sender, "&e- /radiation reload&f - Reload the config.yml file.");
					} else {
						if (args[0].equalsIgnoreCase("reload")) {
							if (sender.hasPermission("rmc.region.reload")) {
								main.reload();
								Util.msg(sender, "&aRadiation Configuration has been reloaded!");
							}
						} else if (args[0].equalsIgnoreCase("set") || args[0].equalsIgnoreCase("toggle")) {
							if (sender.hasPermission("rmc.region.set")) {
								if (args.length == 2) {
									String regionId = args[1];
									main.getRadiationManager().toggleRadiation(sender, regionId);
								} else {
									Util.msg(sender,
											"&e- /radiation set/toggle <regionName>&f - Toggles active radiation in the region.");
								}
							}
						}
					}
				}
			}
		});

		return true;
	}

}
