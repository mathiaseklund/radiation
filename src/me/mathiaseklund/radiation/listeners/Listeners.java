package me.mathiaseklund.radiation.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import me.mathiaseklund.radiation.Main;
import me.mathiaseklund.radiation.utils.Util;

public class Listeners implements Listener {

	Main main = Main.getMain();

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
			public void run() {
				if (!event.getPlayer().hasPermission("rmc.region.immune")) {
					Location loc = player.getLocation();
					boolean radiated = main.getRadiationManager().isRadiationLocation(loc);
					if (radiated) {
						main.getRadiationManager().killQueue.add(player);
					}
				}
			}
		}, 20);
	}

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event) {
		Location to = event.getTo();
		Location from = event.getFrom();
		Player player = event.getPlayer();

		if (to.getBlockX() != from.getBlockX() || to.getBlockY() != from.getBlockY()
				|| to.getBlockZ() != from.getBlockZ()) {
			// Util.debug("Loc Changed");
			Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
				public void run() {
					boolean radiatedTo = main.getRadiationManager().isRadiationLocation(to);
					if (radiatedTo) {
						// Util.debug("location is radiated");
						if (!main.getRadiationManager().isInRadiationRegion(player.getUniqueId())) {
							Util.debug("Moved into radiation zone.");
							main.getRadiationManager().enterRadiationArea(player.getUniqueId());
						}
					} else {
						// Util.debug("location is not radiated");
						if (main.getRadiationManager().isInRadiationRegion(player.getUniqueId())) {
							Util.debug("Moved out of radiation zone.");
							main.getRadiationManager().leaveRadiationArea(player.getUniqueId());
						}
					}
				}
			});
		}
	}

	@EventHandler
	public void onPlayerTeleport(PlayerTeleportEvent event) {
		Player player = event.getPlayer();
		Location loc = event.getTo();
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				boolean radiated = main.getRadiationManager().isRadiationLocation(loc);
				if (radiated) {
					if (!main.getRadiationManager().isInRadiationRegion(player.getUniqueId())) {
						main.getRadiationManager().enterRadiationArea(player.getUniqueId());
					}
				} else {
					if (main.getRadiationManager().isInRadiationRegion(player.getUniqueId())) {
						main.getRadiationManager().leaveRadiationArea(player.getUniqueId());
					}
				}
			}
		});
	}

	public void onPlayerDeath(PlayerDeathEvent event) {
		Player player = event.getEntity();
		main.getRadiationManager().resetRadiation(player);
		main.getRadiationManager().killQueue.remove(player);
	}
}
