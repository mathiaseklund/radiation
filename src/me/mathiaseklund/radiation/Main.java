package me.mathiaseklund.radiation;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;

import lombok.Getter;
import me.mathiaseklund.radiation.commands.RadiationCommand;
import me.mathiaseklund.radiation.listeners.Listeners;
import me.mathiaseklund.radiation.radiation.RadiationManager;
import me.mathiaseklund.radiation.utils.Config;

public class Main extends JavaPlugin {

	@Getter
	static Main main;

	@Getter
	Config cfg;

	@Getter
	RadiationManager radiationManager;

	@Getter
	WorldGuardPlugin worldGuard;

	public void onEnable() {
		main = this;

		cfg = new Config();
		radiationManager = new RadiationManager();
		worldGuard = WorldGuardPlugin.inst();

		loadListeners();
		loadCommands();
	}

	void loadListeners() {
		Bukkit.getPluginManager().registerEvents(new Listeners(), main);
	}

	void loadCommands() {
		getCommand("radiation").setExecutor(new RadiationCommand());
	}

	public void reload() {
		cfg = new Config();
	}
}
